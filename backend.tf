terraform {
  backend "s3" {
    bucket = "proj17gitlabbucket" # Replace with your actual S3 bucket name
    key    = "Jenkins/terraform.tfstate"
    region = "us-east-1"
    dynamodb_table = "proj17dbtable"
  }
}
